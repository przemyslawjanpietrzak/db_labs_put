-- 5.1
CREATE OR REPLACE PACKAGE KONWERSJA IS
    FUNCTION CELS_TO_FAHR(CELS NUMBER) RETURN NUMBER;
    FUNCTION FAHR_TO_CELS(FAHR NUMBER) RETURN NUMBER;
END KONWERSJA;
/

CREATE OR REPLACE PACKAGE BODY KONWERSJA IS
    FUNCTION CELS_TO_FAHR(CELS NUMBER) RETURN NUMBER IS
    BEGIN
        RETURN 9/5 * CELS + 32;
    END CELS_TO_FAHR;
    
    FUNCTION FAHR_TO_CELS(FAHR NUMBER) RETURN NUMBER IS
    BEGIN
        RETURN 5/9 * (FAHR - 32);   
    END FAHR_TO_CELS;
END KONWERSJA;
/
SELECT KONWERSJA.FAHR_TO_CELS(212) CELSJUSZ FROM DUAL;
/
SELECT KONWERSJA.CELS_TO_FAHR(0) FAHRENHEIT FROM DUAL; 


-- 5.2
CREATE OR REPLACE PACKAGE ZMIENNE IS
    vLicznik NUMERIC := 0;
    PROCEDURE ZwiekszLicznik; 
    PROCEDURE ZmniejszLicznik; 
    FUNCTION PokazLicznik RETURN NUMERIC; 
END ZMIENNE;
/

CREATE OR REPLACE PACKAGE BODY ZMIENNE IS  
    PROCEDURE ZwiekszLicznik IS 
    BEGIN
        vLicznik := vLicznik + 1;
        dbms_output.put_line('Zwiêkszono'); 
    END ZwiekszLicznik;
    
    PROCEDURE ZmniejszLicznik IS  
    BEGIN
        vLicznik := vLicznik - 1;
        dbms_output.put_line('Zmniejszono'); 
    END ZmniejszLicznik;
    
    FUNCTION PokazLicznik RETURN NUMERIC IS 
    BEGIN
        RETURN vLicznik;
    END PokazLicznik;
    
    BEGIN
        vLicznik := 1;
        dbms_output.put_line('Zainicjowano'); 
END ZMIENNE;
/

begin
    dbms_output.put_line(Zmienne.PokazLicznik);
end; 
/
begin
    Zmienne.ZwiekszLicznik;
    dbms_output.put_line(Zmienne.PokazLicznik);
    Zmienne.ZwiekszLicznik;
    dbms_output.put_line(Zmienne.PokazLicznik);
end; 
/
begin
    dbms_output.put_line(Zmienne.PokazLicznik);
    Zmienne.ZmniejszLicznik;
    dbms_output.put_line(Zmienne.PokazLicznik);
end; 
/

-- 5.3
CREATE OR REPLACE PROCEDURE IleRekordow (nazwa_relacji IN VARCHAR2) IS
    liczba NUMBER;
    sql_stmt VARCHAR2(100);
BEGIN
    sql_stmt := 'SELECT COUNT(*) FROM ' || nazwa_relacji;
    EXECUTE IMMEDIATE sql_stmt INTO liczba;
    DBMS_OUTPUT.PUT_LINE( 'Liczba rekordow relacji ' || nazwa_relacji || ': ' || liczba);
END IleRekordow;
/

begin
    IleRekordow('PRACOWNICY');
    IleRekordow('ZESPOLY');
end; 
/

-- 5.4
CREATE OR REPLACE PACKAGE MODYFIKACJE IS
    PROCEDURE DodajKolumne (relacja IN VARCHAR2, kolumna IN VARCHAR2, typ_wartoœci IN VARCHAR2);
    PROCEDURE UsunKolumne (relacja IN VARCHAR2, kolumna IN VARCHAR2);
    PROCEDURE ZmienTypKolumny (relacja IN VARCHAR2, kolumna IN VARCHAR2, typ_wartoœci IN VARCHAR2, czy_zachowaæ_dane IN BOOLEAN);
END MODYFIKACJE;
/

CREATE OR REPLACE PACKAGE BODY MODYFIKACJE IS
    PROCEDURE DodajKolumne (relacja IN VARCHAR2, kolumna IN VARCHAR2, typ_wartoœci IN VARCHAR2) IS sql_stmt VARCHAR2 (100);
    BEGIN
        sql_stmt:= 'ALTER TABLE ' || relacja || ' ADD ' || kolumna || ' ' || typ_wartoœci;
        EXECUTE IMMEDIATE sql_stmt;
    END DodajKolumne;
    
    PROCEDURE UsunKolumne (relacja IN VARCHAR2, kolumna IN VARCHAR2) IS sql_stmt VARCHAR2 (100);
    BEGIN
        sql_stmt:= 'ALTER TABLE ' || relacja ||' DROP COLUMN ' || kolumna;
        EXECUTE IMMEDIATE sql_stmt;
    END UsunKolumne;
    
    PROCEDURE ZmienTypKolumny (relacja IN VARCHAR2, kolumna IN VARCHAR2, typ_wartoœci IN VARCHAR2, czy_zachowaæ_dane IN BOOLEAN) IS sql_stmt VARCHAR2 (100);
    BEGIN 
        IF (czy_zachowaæ_dane) THEN
        sql_stmt:= 'ALTER TABLE ' || relacja ||' MODIFY ' || kolumna || ' ' || typ_wartoœci;
        EXECUTE IMMEDIATE sql_stmt;
        ELSE
        UsunKolumne (relacja, kolumna);
        DodajKolumne (relacja, kolumna, typ_wartoœci);
        END IF;
    END ZmienTypKolumny;
END MODYFIKACJE;
/

select * from pracownicy;
/
begin
MODYFIKACJE.DodajKolumne('pracownicy', 'ABC', 'BLOB');
end;
/
begin
MODYFIKACJE.UsunKolumne('pracownicy', 'ABC');
end;
/
