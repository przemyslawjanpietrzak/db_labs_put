--1
CREATE OR REPLACE VIEW
    asystenci (id, nazwisko, placa, staz, staz_pracy)
AS SELECT
    id_prac, nazwisko, placa_pod, zatrudniony, 'years ' || (EXTRACT (YEAR FROM SYSDATE) - EXTRACT (YEAR FROM zatrudniony)) || ' months: ' || (EXTRACT (MONTH FROM SYSDATE) - EXTRACT (MONTH FROM zatrudniony)) FROM pracownicy WHERE etat = 'ASYSTENT';

SELECT * FROM asystenci ORDER BY nazwisko;

--2
CREATE OR REPLACE VIEW
    place (id_zesp, srednia, minimum, maximum, fundusz, l_pencji, l_dodatkow)
AS SELECT
    id_zesp, AVG(placa_pod + NVL(placa_dod, 0)), MIN(placa_pod + NVL(placa_dod, 0)), MAX(placa_pod + NVL(placa_dod, 0)), SUM(placa_pod + NVL(placa_dod, 0)), COUNT(placa_pod), COUNT(placa_dod)
FROM pracownicy
GROUP BY id_zesp;

--3
SELECT
    nazwisko, placa_pod
FROM pracownicy pr
RIGHT JOIN
    place pl ON (pr.placa_pod < pl.srednia)
    AND pr.id_zesp = pl.id_zesp;

--4
CREATE OR REPLACE VIEW place_minimalne AS
SELECT id_prac, nazwisko, etat, placa_pod FROM pracownicy WHERE placa_pod < 700 WITH CHECK OPTION CONSTRAINT za_wysoka_placa;
SELECT * FROM place_minimalne;

--5
UPDATE place_minimalne SET placa_pod = 800 WHERE nazwisko = 'HAPKE';

--6
CREATE OR REPLACE VIEW prac_szef AS
SELECT
    p1.id_prac, p1.id_szefa, p1.nazwisko AS pracownik, p1.etat, p2.nazwisko AS
        szef FROM pracownicy p1 JOIN pracownicy p2 ON p1.id_szefa = p2.id_prac;
SELECT * FROM prac_szef;
INSERT INTO
    PRAC_SZEF (ID_PRAC, ID_SZEFA, PRACOWNIK, ETAT) VALUES (280,150, 'MORZY','ASYSTENT'); 
UPDATE PRAC_SZEF SET ID_SZEFA = 130 WHERE ID_PRAC = 280; 
DELETE FROM PRAC_SZEF WHERE ID_PRAC = 280; 

--7 
CREATE OR REPLACE VIEW zarobki AS
SELECT
    p.id_prac, p.nazwisko, p.etat, p.placa_pod
FROM
    pracownicy p WHERE p.placa_pod < (SELECT s.placa_pod FROM pracownicy s WHERE p.id_szefa = s.id_prac) WITH CHECK OPTION CONSTRAINT placa_wyzsza_od_szefa;
SELECT * FROM zarobki;
UPDATE zarobki SET placa_pod = 100 WHERE nazwisko = 'BIALY'; 

--8
SELECT column_name, updatable, insertable, deletable FROM USER_UPDATABLE_COLUMNS WHERE TABLE_NAME = 'PRAC_SZEF';

--9
SELECT ROWNUM AS ranking, nazwisko, placa_pod FROM pracownicy WHERE ROWNUM < 4 ORDER BY placa_pod DESC;

--10
SELECT * FROM
    (SELECT
        ROWNUM AS ranking,
        nazwisko, placa_pod FROM pracownicy ORDER BY placa_pod DESC
    )
WHERE ranking BETWEEN 5 AND 10;
