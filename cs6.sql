-- 6.1
CREATE SEQUENCE seq_zespoly START WITH 60 INCREMENT BY 10;
/

CREATE OR REPLACE TRIGGER auto_id_zesp
BEFORE INSERT ON zespoly
FOR EACH ROW
BEGIN
    IF( :NEW.id_zesp IS NULL ) THEN
        SELECT seq_zespoly.NEXTVAL INTO :NEW.id_zesp FROM dual;
    END IF;
END;
/

INSERT INTO ZESPOLY(NAZWA) VALUES('KRYPTOGRAFIA'); 
/
INSERT INTO ZESPOLY(NAZWA) SELECT substr('NOWE '||NAZWA,1,20) FROM ZESPOLY WHERE ID_ZESP in (10,20); 
/

-- 6.2
CREATE OR REPLACE TRIGGER auto_liczba_prac
AFTER INSERT OR UPDATE OR DELETE ON pracownicy
FOR EACH ROW
BEGIN
    IF INSERTING OR UPDATING THEN
        UPDATE zespoly
        SET liczba_pracownikow = liczba_pracownikow + 1
        WHERE :NEW.id_zesp = id_zesp;
    ELSIF DELETING THEN
        UPDATE zespoly
        SET liczba_pracownikow = liczba_pracownikow - 1
        WHERE :OLD.id_zesp = id_zesp;
    END IF;
END;
/

ALTER TABLE ZESPOLY ADD (LICZBA_PRACOWNIKOW NUMBER); 
/
UPDATE ZESPOLY Z
SET LICZBA_PRACOWNIKOW = (SELECT COUNT(*) FROM PRACOWNICY WHERE ID_ZESP = Z.ID_ZESP); 
/
SELECT * FROM zespoly; 
/
INSERT INTO pracownicy(ID_PRAC,NAZWISKO,ID_ZESP,ID_SZEFA) VALUES(300, 'NOWY PRACOWNIK', 40, 120); 
/
UPDATE PRACOWNICY SET ID_ZESP = 10 WHERE ID_ZESP = 30;
/

-- 6.3
CREATE TABLE HISTORIA (
    id_prac NUMBER,
    placa_pod NUMBER,
    etat VARCHAR2 (20),
    zespol VARCHAR2 (20),
    modyfikacja DATE
);
/

CREATE OR REPLACE TRIGGER auto_historia
AFTER UPDATE OR DELETE ON pracownicy
FOR EACH ROW
BEGIN
    INSERT INTO historia( id_prac, placa_pod, etat, zespol, modyfikacja )
    VALUES ( :OLD.id_prac, :OLD.placa_pod, :OLD.etat, ( SELECT nazwa FROM zespoly WHERE id_zesp = :OLD.id_zesp ), CURRENT_DATE );
END;
/

UPDATE PRACOWNICY SET PLACA_POD = 800 WHERE NAZWISKO = 'KROLIKOWSKI'; 
/
DELETE FROM PRACOWNICY WHERE NAZWISKO = 'HAPKE'; 
/
SELECT * FROM HISTORIA; 
/

-- 6.4
CREATE OR REPLACE VIEW SZEFOWIE (szef, pracownicy)
AS
    SELECT p.nazwisko, COUNT (p.nazwisko) 
    FROM pracownicy p, pracownicy s 
    WHERE p.id_prac = s.id_szefa 
    GROUP BY p.nazwisko;
/

CREATE OR REPLACE TRIGGER auto_szef
INSTEAD OF DELETE ON SZEFOWIE
FOR EACH ROW
DECLARE
  v_id_prac NUMBER;
BEGIN
  SELECT id_prac INTO v_id_prac FROM pracownicy WHERE nazwisko = :OLD.szef;
  DELETE FROM pracownicy
  WHERE id_prac = v_id_prac OR id_szefa = v_id_prac;
END;
/

SELECT * FROM szefowie; 
/
SELECT * FROM pracownicy WHERE id_prac = 140 OR id_szefa = 140; 
/
DELETE FROM szefowie WHERE szef='MORZY'; 
/

-- 6.5
ALTER TABLE pracownicy DROP CONSTRAINT fk_id_szefa;
/
ALTER TABLE pracownicy ADD CONSTRAINT fk_id_szefa FOREIGN KEY (id_szefa) REFERENCES pracownicy (id_prac) ON DELETE CASCADE;
/

CREATE OR REPLACE TRIGGER usun_prac
AFTER DELETE ON pracownicy
FOR EACH ROW
BEGIN
    DBMS_OUTPUT.PUT_LINE (:OLD.nazwisko);
END;
/

CREATE OR REPLACE TRIGGER usun_prac
BEFORE DELETE ON pracownicy
FOR EACH ROW
BEGIN
    DBMS_OUTPUT.PUT_LINE (:OLD.nazwisko);
END;
/