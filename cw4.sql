--
SELECT nazwisko, placa_pod FROM pracownicy WHERE id_zesp = 10;
/
EXECUTE podwyzka(10, 10);
/

EXECUTE podwyzka(200, 10);
/

--4.1 4.2
CREATE OR REPLACE PROCEDURE podwyzka (
    p_id_zesp IN NUMBER,
    procent IN NUMBER DEFAULT 15
) IS
CURSOR k_pracownicy IS
SELECT * FROM pracownicy
WHERE id_zesp = p_id_zesp
FOR UPDATE;
    istnieje BOOLEAN:= FALSE;
    BEGIN
    FOR pracownik IN k_pracownicy
    LOOP    
        UPDATE pracownicy SET placa_pod = (procent * placa_pod / 100) + placa_pod
        WHERE CURRENT OF k_pracownicy;
        istnieje:= true;
    END LOOP;
    IF istnieje = false THEN
        RAISE NO_DATA_FOUND;
    END IF;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
        dbms_output.put_line('team doesnt exist');
END;
/

--
VARIABLE v_pracownicy NUMBER;
/
VARIABLE v_zespol VARCHAR2(20);
/
EXECUTE :v_zespol:= 'ADMINISTRACJA';
/
EXECUTE liczba_pracownikow(:v_zespol,:v_pracownicy);
/
PRINT v_pracownicy;
/

-- 4.3
CREATE OR REPLACE PROCEDURE liczba_pracownikow (
    p_nazwa_zesp IN zespoly.nazwa%TYPE,
    p_liczba_pracownikow OUT NUMBER
) IS
CURSOR K_pracownicy IS
SELECT * FROM pracownicy p INNER JOIN zespoly z ON p.id_zesp = z.id_zesp
WHERE z.nazwa = p_nazwa_zesp;
istnieje BOOLEAN:= FALSE;
BEGIN
    p_liczba_pracownikow:= 0;
    FOR pracownik IN k_pracownicy
    LOOP
        istnieje:= TRUE;
        p_liczba_pracownikow := p_liczba_pracownikow  + 1;
    END LOOP;
    IF istnieje = FALSE THEN
        RAISE NO_DATA_FOUND;
    END IF;
        EXCEPTION
    WHEN NO_DATA_FOUND THEN
        dbms_output.put_line('Team doesnt exist');
END;
/

--
EXEC nowy_pracownik('DYNDALSKI','ALGORYTMY','BLAZEWICZ',250); 
/
SELECT * FROM pracownicy WHERE nazwisko = 'DYNDALSKI'; 
/

--4
CREATE OR REPLACE PROCEDURE nowy_pracownik (
    p_nazwisko IN pracownicy.nazwisko%TYPE,
    p_nazwa_zesp IN zespoly.nazwa%TYPE,
    p_nazwisko_szefa IN pracownicy.nazwisko%TYPE,
    p_placa_pod IN pracownicy.placa_pod%TYPE,
    p_zatrudniony IN pracownicy.zatrudniony%TYPE DEFAULT SYSDATE,
    p_etat IN Pracownicy.Etat%TYPE DEFAULT 'STAZYSTA'
) IS

BRAK_ZESPOLU EXCEPTION;
BRAK_PRACOWNIKA EXCEPTION;
x_id_zesp zespoly.id_zesp%TYPE;
x_id_szefa pracownicy.id_prac%TYPE;
x_id_prac pracownicy.id_prac%TYPE;
licznik NUMBER;

BEGIN 
    SELECT COUNT(*) INTO licznik
    FROM zespoly
    WHERE nazwa = p_nazwa_zesp;
    IF licznik = 0 THEN
    RAISE BRAK_ZESPOLU;
    END IF;

    SELECT COUNT(*) INTO licznik FROM pracownicy
    WHERE nazwisko = p_nazwisko_szefa;
    IF licznik = 0 THEN
    RAISE BRAK_PRACOWNIKA;
    END IF;

    SELECT id_zesp INTO x_id_zesp FROM zespoly
    WHERE nazwa = p_nazwa_zesp;

    SELECT id_prac INTO x_id_szefa FROM pracownicy
    WHERE nazwisko = p_nazwisko_szefa;

    SELECT MAX(id_prac)+1 INTO x_id_prac FROM pracownicy;

    INSERT INTO pracownicy (id_prac, nazwisko, etat, id_szefa, zatrudniony, placa_pod, id_zesp)
    VALUES (x_id_prac, p_nazwisko, p_etat, x_id_szefa, p_zatrudniony, p_placa_pod, x_id_zesp);

    EXCEPTION
    WHEN brak_zespolu THEN
        dbms_output.put_line('team doesnt exist');
    WHEN brak_pracownika THEN
        dbms_output.put_line('boss doesnt exist');
END;
/


--
SELECT nazwisko, placa_pod brutto, placa_netto(placa_pod, 23) netto FROM pracownicy WHERE etat = 'PROFESOR'; 
/

--5
CREATE OR REPLACE FUNCTION placa_netto (
    brutto IN NUMBER,
    podatek IN NUMBER DEFAULT 20
) RETURN NUMBER IS

BEGIN
    RETURN (100 * brutto) / (100 + podatek);
END;
/

--
SELECT silnia(8) FROM DUAL; 
/

--6
CREATE OR REPLACE FUNCTION silnia (
  nn IN NUMBER
) RETURN NUMBER IS

silnia NUMBER := 1;
--
SELECT nazwisko, placa_pod FROM pracownicy WHERE id_zesp = 10;
/
--
EXECUTE podwyzka(10, 10);
/
--
EXECUTE podwyzka(200, 10);
/

--1 --2
CREATE OR REPLACE PROCEDURE podwyzka (
p_id_zesp IN NUMBER,
procent IN NUMBER DEFAULT 15
) IS
CURSOR k_pracownicy IS
SELECT * FROM pracownicy
WHERE id_zesp = p_id_zesp
FOR UPDATE;
istnieje BOOLEAN:= FALSE;
BEGIN
  FOR pracownik IN k_pracownicy
  LOOP    
    UPDATE pracownicy SET placa_pod = (proce--
SELECT nazwisko, placa_pod FROM pracownicy WHERE id_zesp = 10;
/
--
EXECUTE podwyzka(10, 10);
/
--
EXECUTE podwyzka(200, 10);
/

--1 --2
CREATE OR REPLACE PROCEDURE podwyzka (
p_id_zesp IN NUMBER,
procent IN NUMBER DEFAULT 15
) IS
CURSOR k_pracownicy IS
SELECT * FROM pracownicy
WHERE id_zesp = p_id_zesp
FOR UPDATE;
istnieje BOOLEAN:= FALSE;
BEGIN
  FOR pracownik IN k_pracownicy
  LOOP    
    UPDATE pracownicy SET placa_pod = (procent * placa_pod / 100) + placa_pod
    WHERE CURRENT OF k_pracownicy;
    istnieje:= true;
  END LOOP;
  IF istnieje = false THEN
    RAISE NO_DATA_FOUND;
  END IF;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      dbms_output.put_line('Brak zespolu o podanym numerze!');
END;
/

--
VARIABLE v_pracownicy NUMBER;
/
VARIABLE v_zespol VARCHAR2(20);
/
EXECUTE :v_zespol:= 'ADMINISTRACJA';
/
EXECUTE liczba_pracownikow(:v_zespol,:v_pracownicy);
/
PRINT v_pracownicy;
/

--3
CREATE OR REPLACE PROCEDURE liczba_pracownikow (
    p_nazwa_zesp IN zespoly.nazwa%TYPE,
    p_liczba_pracownikow OUT NUMBER
) IS
CURSOR K_pracownicy IS
SELECT * FROM pracownicy p INNER JOIN zespoly z ON p.id_zesp = z.id_zesp
WHERE z.nazwa = p_nazwa_zesp;
istnieje BOOLEAN:= FALSE;
BEGIN
  p_liczba_pracownikow:= 0;
  FOR pracownik IN k_pracownicy
  LOOP
    istnieje:= TRUE;
    p_liczba_pracownikow := p_liczba_pracownikow  + 1;
  END LOOP;
  IF istnieje = FALSE THEN
    RAISE NO_DATA_FOUND;
  END IF;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      dbms_output.put_line('Brak zespolu o podanym numerze!');
END;
/

--
EXEC nowy_pracownik('DYNDALSKI','ALGORYTMY','BLAZEWICZ',250); 
/
SELECT * FROM pracownicy WHERE nazwisko = 'DYNDALSKI'; 
/

--4
CREATE OR REPLACE PROCEDURE nowy_pracownik (
    p_nazwisko IN pracownicy.nazwisko%TYPE,
    p_nazwa_zesp IN zespoly.nazwa%TYPE,
    p_nazwisko_szefa IN pracownicy.nazwisko%TYPE,
    p_placa_pod IN pracownicy.placa_pod%TYPE,
    p_zatrudniony IN pracownicy.zatrudniony%TYPE DEFAULT SYSDATE,
    p_etat IN Pracownicy.Etat%TYPE DEFAULT 'STAZYSTA'
) IS

BRAK_ZESPOLU EXCEPTION;
BRAK_PRACOWNIKA EXCEPTION;
x_id_zesp zespoly.id_zesp%TYPE;
x_id_szefa pracownicy.id_prac%TYPE;
x_id_prac pracownicy.id_prac%TYPE;
licznik NUMBER;

BEGIN 
    SELECT COUNT(*) INTO licznik
    FROM zespoly
    WHERE nazwa = p_nazwa_zesp;
    IF licznik = 0 THEN
        RAISE BRAK_ZESPOLU;
    END IF;
    
    SELECT COUNT(*) INTO licznik FROM pracownicy
    WHERE nazwisko = p_nazwisko_szefa;
    IF licznik = 0 THEN
        RAISE BRAK_PRACOWNIKA;
    END IF;
    
    SELECT id_zesp INTO x_id_zesp FROM zespoly
    WHERE nazwa = p_nazwa_zesp;
    
    SELECT id_prac INTO x_id_szefa FROM pracownicy
    WHERE nazwisko = p_nazwisko_szefa;
    
    SELECT MAX(id_prac)+1 INTO x_id_prac FROM pracownicy;

    INSERT INTO pracownicy (id_prac, nazwisko, etat, id_szefa, zatrudniony, placa_pod, id_zesp)
    VALUES (x_id_prac, p_nazwisko, p_etat, x_id_szefa, p_zatrudniony, p_placa_pod, x_id_zesp);

    EXCEPTION
        WHEN brak_zespolu THEN
        dbms_output.put_line('Nie znaleziono zespolu');
        WHEN brak_pracownika THEN
        dbms_output.put_line('Nie znaleziono szefa');
END;
/


--
SELECT nazwisko, placa_pod brutto, placa_netto(placa_pod, 23) netto FROM pracownicy WHERE etat = 'PROFESOR'; 
/

--5
CREATE OR REPLACE FUNCTION placa_netto (
    brutto IN NUMBER,
    podatek IN NUMBER DEFAULT 20
) RETURN NUMBER IS

BEGIN
  RETURN (100 * brutto) / (100 + podatek);
END;
/

--
SELECT silnia(8) FROM DUAL; 
/

--6
CREATE OR REPLACE FUNCTION silnia (
  nn IN NUMBER
) RETURN NUMBER IS

silnia NUMBER := 1;

BEGIN 
    IF nn = 0 THEN
    RETURN 1;
    END IF;

    FOR i IN 1..nn
    LOOP
        silnia := silnia * I;
    END LOOP; 
    RETURN silnia;
END;
/

--
SELECT silnia2(10) FROM DUAL; 
/

--7
CREATE OR REPLACE FUNCTION silnia2 (
    nn IN NUMBER
) RETURN NUMBER IS

BEGIN 
    IF nn = 0 THEN
        RETURN 1;
    END IF;

    RETURN nn*silnia2(nn-1);
END;
/

--
SELECT nazwisko, zatrudniony, staz(zatrudniony) FROM pracownicy WHERE placa_pod > 1000;
/

--8
CREATE OR REPLACE FUNCTION staz (
    p_zatrudniony IN DATE
) RETURN NUMBER IS
BEGIN
    RETURN floor(months_between(SYSDATE, p_zatrudniony)/12);
END;
/nt * placa_pod / 100) + placa_pod
    WHERE CURRENT OF k_pracownicy;
        istnieje:= true;
    END LOOP;
    IF istnieje = false THEN
        RAISE NO_DATA_FOUND;
    END IF;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
        dbms_output.put_line('Brak zespolu o podanym numerze!');
END;
/

--
VARIABLE v_pracownicy NUMBER;
/
VARIABLE v_zespol VARCHAR2(20);
/
EXECUTE :v_zespol:= 'ADMINISTRACJA';
/
EXECUTE liczba_pracownikow(:v_zespol,:v_pracownicy);
/
PRINT v_pracownicy;
/

--3
CREATE OR REPLACE PROCEDURE liczba_pracownikow (
    p_nazwa_zesp IN zespoly.nazwa%TYPE,
    p_liczba_pracownikow OUT NUMBER
) IS
CURSOR K_pracownicy IS
SELECT * FROM pracownicy p INNER JOIN zespoly z ON p.id_zesp = z.id_zesp
WHERE z.nazwa = p_nazwa_zesp;
istnieje BOOLEAN:= FALSE;
BEGIN
  p_liczba_pracownikow:= 0;
    FOR pracownik IN k_pracownicy
    LOOP
        istnieje:= TRUE;
        p_liczba_pracownikow := p_liczba_pracownikow  + 1;
    END LOOP;
    IF istnieje = FALSE THEN
        RAISE NO_DATA_FOUND;
    END IF;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
        dbms_output.put_line('Brak zespolu o podanym numerze!');
END;
/

--
EXEC nowy_pracownik('DYNDALSKI','ALGORYTMY','BLAZEWICZ',250); 
/
SELECT * FROM pracownicy WHERE nazwisko = 'DYNDALSKI'; 
/

--4
CREATE OR REPLACE PROCEDURE nowy_pracownik (
    p_nazwisko IN pracownicy.nazwisko%TYPE,
    p_nazwa_zesp IN zespoly.nazwa%TYPE,
    p_nazwisko_szefa IN pracownicy.nazwisko%TYPE,
    p_placa_pod IN pracownicy.placa_pod%TYPE,
    p_zatrudniony IN pracownicy.zatrudniony%TYPE DEFAULT SYSDATE,
    p_etat IN Pracownicy.Etat%TYPE DEFAULT 'STAZYSTA'
) IS

BRAK_ZESPOLU EXCEPTION;
BRAK_PRACOWNIKA EXCEPTION;
x_id_zesp zespoly.id_zesp%TYPE;
x_id_szefa pracownicy.id_prac%TYPE;
x_id_prac pracownicy.id_prac%TYPE;
licznik NUMBER;

BEGIN 
    SELECT COUNT(*) INTO licznik
    FROM zespoly
    WHERE nazwa = p_nazwa_zesp;
    IF licznik = 0 THEN
        RAISE BRAK_ZESPOLU;
    END IF;
    
    SELECT COUNT(*) INTO licznik FROM pracownicy
    WHERE nazwisko = p_nazwisko_szefa;
    IF licznik = 0 THEN
        RAISE BRAK_PRACOWNIKA;
    END IF;
    
    SELECT id_zesp INTO x_id_zesp FROM zespoly
    WHERE nazwa = p_nazwa_zesp;
    
    SELECT id_prac INTO x_id_szefa FROM pracownicy
    WHERE nazwisko = p_nazwisko_szefa;
    
    SELECT MAX(id_prac)+1 INTO x_id_prac FROM pracownicy;

    INSERT INTO pracownicy (id_prac, nazwisko, etat, id_szefa, zatrudniony, placa_pod, id_zesp)
    VALUES (x_id_prac, p_nazwisko, p_etat, x_id_szefa, p_zatrudniony, p_placa_pod, x_id_zesp);

    EXCEPTION
        WHEN brak_zespolu THEN
        dbms_output.put_line('Nie znaleziono zespolu');
        WHEN brak_pracownika THEN
        dbms_output.put_line('Nie znaleziono szefa');
END;
/


--
SELECT nazwisko, placa_pod brutto, placa_netto(placa_pod, 23) netto FROM pracownicy WHERE etat = 'PROFESOR'; 
/

--5
CREATE OR REPLACE FUNCTION placa_netto (
    brutto IN NUMBER,
    podatek IN NUMBER DEFAULT 20
) RETURN NUMBER IS

BEGIN
  RETURN (100 * brutto) / (100 + podatek);
END;
/

--
SELECT silnia(8) FROM DUAL; 
/

--6
CREATE OR REPLACE FUNCTION silnia (
    nn IN NUMBER
) RETURN NUMBER IS

silnia NUMBER := 1;

BEGIN 
    IF nn = 0 THEN
        RETURN 1;
    END IF;

    FOR i IN 1..nn
        LOOP
            silnia := silnia * I;
        END LOOP; 
    RETURN silnia;
END;
/

--
SELECT silnia2(10) FROM DUAL; 
/

--7
CREATE OR REPLACE FUNCTION silnia2 (
  nn IN NUMBER
) RETURN NUMBER IS

BEGIN 
    IF nn = 0 THEN
        RETURN 1;
    END IF;

    RETURN nn*silnia2(nn-1);
END;
/

--
SELECT nazwisko, zatrudniony, staz(zatrudniony) FROM pracownicy WHERE placa_pod > 1000;
/

--8
CREATE OR REPLACE FUNCTION staz (
    p_zatrudniony IN DATE
) RETURN NUMBER IS
BEGIN
    RETURN floor(months_between(SYSDATE, p_zatrudniony)/12);
END;
/
BEGIN 
    IF nn = 0 THEN
        RETURN 1;
    END IF;

    FOR i IN 1..nn
        LOOP
            silnia := silnia * I;
        END LOOP; 
    RETURN silnia;
END;
/

--
SELECT silnia2(10) FROM DUAL; 
/

--7
CREATE OR REPLACE FUNCTION silnia2 (
    nn IN NUMBER
) RETURN NUMBER IS

BEGIN 
    IF nn = 0 THEN
        RETURN 1;
    END IF;

    RETURN nn*silnia2(nn-1);
END;
/

--
SELECT nazwisko, zatrudniony, staz(zatrudniony) FROM pracownicy WHERE placa_pod > 1000;
/

--8
CREATE OR REPLACE FUNCTION staz (
    p_zatrudniony IN DATE
) RETURN NUMBER IS
BEGIN
    RETURN floor(months_between(SYSDATE, p_zatrudniony)/12);
END;
/