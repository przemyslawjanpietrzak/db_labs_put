--
ALTER TABLE pracownicy
ADD FOREIGN KEY(id_zesp)
REFERENCES zespoly(id_zesp);
/

-- 3.1
DECLARE 
    v_etat VARCHAR(10):= '&etat';
    CURSOR kursor (p_etat VARCHAR) IS
    SELECT * FROM pracownicy WHERE etat = p_etat;
BEGIN 
    SELECT nazwa INTO v_etat FROM etaty WHERE nazwa = v_etat;
        FOR i IN kursor(v_etat) LOOP
        DBMS_OUTPUT.PUT_LINE(i.nazwisko || ' ' || i.etat);
    END LOOP;
        EXCEPTION
    WHEN NO_DATA_FOUND THEN
        DBMS_OUTPUT.PUT_LINE('Etat ' || v_etat || ' nie istnieje!');
END;
/

-- 3.2
DECLARE
  CURSOR c_szefowie IS 
	SELECT s.id_prac, s.nazwisko AS szef, s.placa_pod AS wyplata_szefa,
	SUM(p.placa_pod) AS wyplata_pracownika
	FROM pracownicy s JOIN pracownicy p ON s.id_prac = p.id_szefa 
	WHERE s.etat = 'PROFESOR'
	GROUP BY s.id_prac, s.nazwisko, s.placa_pod;
BEGIN
    FOR szef IN c_szefowie
    LOOP
    	IF ((szef.wyplata_szefa + 0.1 * szef.wyplata_pracownika) > 2000)
    	THEN
    		RAISE_APPLICATION_ERROR(-20010, 'wiecej niz 2000!');
    	ELSE
    		UPDATE pracownicy
    		SET placa_pod = placa_pod + 0.1 * szef.wyplata_pracownika
    		WHERE id_prac = szef.id_prac;
		END IF;
    END loop;
END;
/

-- 3.3
DECLARE
    p_id_prac NUMBER;
    p_id_zesp NUMBER;
    p_nazwisko VARCHAR(80);
    p_placa_pod NUMBER;
    PRIMARY_KEY_DUPLICATE EXCEPTION;
    PRAGMA EXCEPTION_INIT(PRIMARY_KEY_DUPLICATE, -1);
    WORKER_ID_DOESNT_EXIST EXCEPTION;
    PRAGMA EXCEPTION_INIT(WORKER_ID_DOESNT_EXIST, -1400);
    TOO_LOW_SALARY EXCEPTION;
    PRAGMA EXCEPTION_INIT(TOO_LOW_SALARY, -2290);
    TEAM_DOESNT_EXIST EXCEPTION;
    PRAGMA EXCEPTION_INIT(TEAM_DOESNT_EXIST, -2291);
BEGIN
    p_id_prac:= '&id_prac';
    p_id_zesp:= '&id_zesp';
    p_nazwisko:= '&nazwisko';
    p_placa_pod:= '&placa_pod';
    INSERT INTO pracownicy (id_prac, id_zesp, nazwisko, placa_pod) VALUES (p_id_prac, p_id_zesp, p_nazwisko, p_placa_pod);
EXCEPTION
    WHEN PRIMARY_KEY_DUPLICATE THEN  
        dbms_output.put_line('ID pracownika ju¿ istnieje!');
    WHEN TOO_LOW_SALARY THEN
        dbms_output.put_line('Minimalna paca musi wynosiæ 101!');
    WHEN WORKER_ID_DOESNT_EXIST THEN
        dbms_output.put_line('Podaj ID pracownika!');
    WHEN TEAM_DOESNT_EXIST THEN
        dbms_output.put_line('Podaj poprawn¹ nazwê zespou!');
END;
/

-- 3.4
DECLARE
    p_nazwisko VARCHAR(80);
    licznik NUMBER;
    BOSS EXCEPTION;
    PRAGMA EXCEPTION_INIT(BOSS, -2292);
BEGIN
    p_nazwisko:= '&nazwisko';
    SELECT COUNT(*) INTO licznik FROM pracownicy WHERE nazwisko = p_nazwisko;
    IF licznik = 0 THEN
        RAISE_APPLICATION_ERROR(-20020, 'doesnt exist');
    ELSIF licznik > 1 THEN
        RAISE_APPLICATION_ERROR(-20030, 'more than one!');
    ELSE
        DELETE FROM pracownicy WHERE nazwisko = p_nazwisko;
    END IF;
EXCEPTION
    WHEN BOSS THEN
        RAISE_APPLICATION_ERROR(-20040, 'can not remove');
END;
/
